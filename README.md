A repo containing my research on the planning problem.

Excellent lectures on the topic:
https://algo2.iti.kit.edu/balyo/plan/?fbclid=IwAR228-l9anDy5jGU_2eA34-arvvcX6qbsN0BDr7RMWcz8M2Lrd6vu4CwgHw


PDDL toy problems forked from:
https://github.com/oscar-lima/pddl_problems

For ease of use, it is recommended to use the PDDL plugin from VSCode Marketplace.

An overview of available solvers:
https://algo2.iti.kit.edu/balyo/plan/files/getting-started-with-planning.pdf

Example run of FF on one of the trucking problems
```
./Metric-FF/ff -p pddl_problems/ipc_transport/ -o domain.pddl -f p01.pddl
```
To make FF optimize the total cost instead of plan length add the -O flag:
```
./Metric-FF/ff -p pddl_problems/ipc_transport/ -o domain.pddl -f p01.pddl -O
```

Example run of Madagascar (SAT solver) on the same problem:
```
./M pddl_problems/ipc_transport/domain.pddl pddl_problems/ipc_transport/problems/p01.pddl -Q
```